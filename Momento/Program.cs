﻿using System;

namespace TextEditor
{
    class Program
    {
        static void Main(string[] args)
        {
            // Створення екземпляру текстового редактора
            var editor = new TextEditor();

            // Додавання контенту до документа
            editor.AddContent("This is the initial content.");
            editor.PrintContent();

            // Виконання збереження та відновлення стану
            editor.Save();
            editor.AddContent("This is additional content.");
            editor.PrintContent();
            editor.Undo();
            editor.PrintContent();
        }
    }
}
