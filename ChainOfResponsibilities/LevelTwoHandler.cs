using System;

public class LevelTwoHandler : Handler
{
    public override void HandleRequest(string request)
    {
        if (request == "2")
        {
            Console.WriteLine("Level 2: Handling request and resolving issue.");
        }
        else if (nextHandler != null)
        {
            nextHandler.HandleRequest(request);
        }
    }
}