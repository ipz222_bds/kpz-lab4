namespace DesignPatterns.Mediator
{
    public class Aircraft
    {
        public string Name { get; }
        public CommandCentre CommandCentre { get; }
        public bool IsTakingOff { get; set; }

        public Aircraft(string name, CommandCentre commandCentre)
        {
            this.Name = name;
            this.CommandCentre = commandCentre;
        }

        public void RequestLanding()
        {
            Console.WriteLine($"Aircraft {this.Name} requests landing.");
            CommandCentre.LandAircraft(this);
        }

        public void RequestTakeOff()
        {
            Console.WriteLine($"Aircraft {this.Name} requests takeoff.");
            CommandCentre.TakeOffAircraft(this);
        }
    }
}