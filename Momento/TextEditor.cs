using System;
using System.Collections.Generic;

namespace TextEditor
{
    // Клас, який представляє текстовий редактор
    public class TextEditor
    {
        // Поточний документ
        private TextDocument _document;

        // Список збережених станів документа
        private readonly Stack<TextDocumentMemento> _savedStates = new Stack<TextDocumentMemento>();

        // Конструктор
        public TextEditor()
        {
            _document = new TextDocument("");
        }

        // Збереження поточного стану документа
        public void Save()
        {
            _savedStates.Push(_document.CreateMemento());
        }

        // Скасування останньої зміни
        public void Undo()
        {
            if (_savedStates.Count > 0)
            {
                var memento = _savedStates.Pop();
                memento.Restore(_document);
            }
            else
            {
                Console.WriteLine("Cannot undo. No saved states available.");
            }
        }

        // Виведення вмісту документа
        public void PrintContent()
        {
            _document.Print();
        }

        // Встановлення нового вмісту документа
        public void AddContent(string content)
        {
            _document.Content += content;
        }
    }
}
