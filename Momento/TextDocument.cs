using System;

namespace TextEditor
{
    // Клас для представлення текстового документу
    public class TextDocument
    {
        public string Content { get; set; }

        public TextDocument(string content)
        {
            Content = content;
        }

        public void Print()
        {
            Console.WriteLine(Content);
        }

        public TextDocumentMemento CreateMemento()
        {
            return new TextDocumentMemento(Content);
        }
    }
}
