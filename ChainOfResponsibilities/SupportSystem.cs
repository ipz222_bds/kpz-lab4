using System;

public class SupportSystem
{
    private Handler firstHandler;

    public SupportSystem()
    {
        // Створення обробників
        firstHandler = new LevelOneHandler();
        var secondHandler = new LevelTwoHandler();
        var thirdHandler = new LevelThreeHandler();
        var fourthHandler = new LevelFourHandler();

        // Встановлення ланцюжка обробників
        firstHandler.SetNextHandler(secondHandler);
        secondHandler.SetNextHandler(thirdHandler);
        thirdHandler.SetNextHandler(fourthHandler);
    }

    public void Start()
    {
        bool needsFurtherAssistance = true;

        while (needsFurtherAssistance)
        {
            Console.WriteLine("Please choose support level (1-4):");
            var input = Console.ReadLine();

            if (input != null)
            {
                firstHandler.HandleRequest(input);
            }
            else
            {
                Console.WriteLine("Invalid input. Please try again.");
            }

            Console.WriteLine("Do you need further assistance? (yes/no)");
            var continueInput = Console.ReadLine();
            if (continueInput?.ToLower() != "yes")
            {
                needsFurtherAssistance = false;
            }
        }
    }
}
