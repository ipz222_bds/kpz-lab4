namespace Task6
{
    public interface IEventListener
    {
        void HandleEvent(Event e);
    }
}