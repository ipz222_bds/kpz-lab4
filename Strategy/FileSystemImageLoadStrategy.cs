using System;
using System.IO;

namespace Task6
{
    public class FileSystemImageLoadStrategy : IImageLoadStrategy
    {
        public string LoadImage(string path)
        {
            if (File.Exists(path))
            {
                return Convert.ToBase64String(File.ReadAllBytes(path));
            }
            else
            {
                throw new FileNotFoundException("File not found", path);
            }
        }
    }
}
