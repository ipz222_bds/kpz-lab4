namespace TextEditor
{
    // Клас збереження стану текстового документу
    public class TextDocumentMemento
    {
        public string Content { get; }

        public TextDocumentMemento(string content)
        {
            Content = content;
        }

        public void Restore(TextDocument document)
        {
            document.Content = Content;
        }
    }
}
