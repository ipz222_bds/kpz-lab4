﻿using System;
using System.Collections.Generic;
using DesignPatterns.Mediator;

public class Program
{
    public static void Main(string[] args)
    {
        Runway runway1 = new Runway();
        Runway runway2 = new Runway();

        List<Runway> runways = new List<Runway> { runway1, runway2 };
        CommandCentre commandCentre = new CommandCentre(runways);

        Aircraft aircraft1 = new Aircraft("Aircraft 1", commandCentre);
        Aircraft aircraft2 = new Aircraft("Aircraft 2", commandCentre);

        commandCentre.RegisterAircraft(aircraft1);
        commandCentre.RegisterAircraft(aircraft2);

        aircraft1.RequestLanding();
        aircraft2.RequestLanding();
        aircraft1.RequestTakeOff();
        aircraft2.RequestTakeOff();
    }
}
