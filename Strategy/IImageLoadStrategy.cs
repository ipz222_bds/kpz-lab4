namespace Task6
{
    public interface IImageLoadStrategy
    {
        string LoadImage(string path);
    }
}