using System;

namespace Task6
{
    public class Event
    {
        public string Type { get; }
        public DateTime Timestamp { get; }

        public Event(string type)
        {
            Type = type;
            Timestamp = DateTime.Now;
        }
    }
}