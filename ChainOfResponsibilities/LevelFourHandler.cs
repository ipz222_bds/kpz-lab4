using System;

public class LevelFourHandler : Handler
{
    public override void HandleRequest(string request)
    {
        if (request == "4")
        {
            Console.WriteLine("Level 4: Handling request and resolving issue.");
        }
        else if (nextHandler != null)
        {
            nextHandler.HandleRequest(request);
        }
        else
        {
            Console.WriteLine("No appropriate handler found. Please try again.");
        }
    }
}
