﻿using System;

namespace Task6
{
    class Program
    {
        static void Main(string[] args)
        {
            LightElementNode div = new LightElementNode("div", true, false);
            div.CssClasses.Add("container");

            LightElementNode p = new LightElementNode("p", true, false);
            p.Children.Add(new LightTextNode("This is a paragraph."));

            IImageLoadStrategy networkStrategy = new NetworkImageLoadStrategy();

            string networkImageUrl = "https://zhzh.com.ua/storage/11068/32743150.jpg";  // Приклад URL для зображення

            LightImageNode imgFromNetwork = new LightImageNode(networkImageUrl, networkStrategy);

            div.Children.Add(p);
            div.Children.Add(imgFromNetwork);

            Console.WriteLine(div.OuterHTML);

            // Додавання подій
            div.AddEventListener("click", new ClickEventListener());
            div.DispatchEvent(new Event("click"));
        }
    }

    class ClickEventListener : IEventListener
    {
        public void HandleEvent(Event e)
        {
            Console.WriteLine($"Event {e.Type} handled at {e.Timestamp}");
        }
    }
}
