using System;

namespace Task6
{
    public class NetworkImageLoadStrategy : IImageLoadStrategy
    {
        public string LoadImage(string url)
        {
            // Тут можна реалізувати завантаження зображення з мережі за URL
            return $"<img src=\"{url}\" />";
        }
    }
}
