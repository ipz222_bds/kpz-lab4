using System;

public abstract class Handler
{
    protected Handler? nextHandler;

    public void SetNextHandler(Handler nextHandler)
    {
        this.nextHandler = nextHandler;
    }

    public abstract void HandleRequest(string request);
}