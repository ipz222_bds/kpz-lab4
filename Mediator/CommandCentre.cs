namespace DesignPatterns.Mediator
{
    public class CommandCentre
    {
        private List<Runway> _runways;
        private List<Aircraft> _aircrafts;

        public CommandCentre(List<Runway> runways)
        {
            this._runways = runways;
            this._aircrafts = new List<Aircraft>();
        }

        public void RegisterAircraft(Aircraft aircraft)
        {
            this._aircrafts.Add(aircraft);
        }

        public void LandAircraft(Aircraft aircraft)
        {
            foreach (var runway in _runways)
            {
                if (runway.IsBusyWithAircraft == null)
                {
                    Console.WriteLine($"Aircraft {aircraft.Name} is landing on runway {runway.Id}.");
                    runway.IsBusyWithAircraft = aircraft;
                    runway.HighLightRed();
                    return;
                }
            }
            Console.WriteLine($"Aircraft {aircraft.Name} could not land, all runways are busy.");
        }

        public void TakeOffAircraft(Aircraft aircraft)
        {
            foreach (var runway in _runways)
            {
                if (runway.IsBusyWithAircraft == aircraft)
                {
                    Console.WriteLine($"Aircraft {aircraft.Name} is taking off from runway {runway.Id}.");
                    runway.IsBusyWithAircraft = null;
                    runway.HighLightGreen();
                    return;
                }
            }
            Console.WriteLine($"Aircraft {aircraft.Name} could not take off, it is not on any runway.");
        }
    }
}
