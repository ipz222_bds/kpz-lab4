namespace Task6
{
    public class LightImageNode : LightNode
    {
        private readonly string _imageUrl;
        private readonly IImageLoadStrategy _imageLoadStrategy;

        public LightImageNode(string imageUrl, IImageLoadStrategy imageLoadStrategy)
        {
            _imageUrl = imageUrl;
            _imageLoadStrategy = imageLoadStrategy;
        }

        public override string OuterHTML => _imageLoadStrategy.LoadImage(_imageUrl);

        public override string InnerHTML => string.Empty;
    }
}
