namespace Task6
{
    public class LightTextNode : LightNode
    {
        public string Text { get; set; }

        public LightTextNode(string text)
        {
            Text = text;
        }

        public override string OuterHTML => InnerHTML;

        public override string InnerHTML => Text;
    }
}