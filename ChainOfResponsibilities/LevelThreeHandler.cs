using System;

public class LevelThreeHandler : Handler
{
    public override void HandleRequest(string request)
    {
        if (request == "3")
        {
            Console.WriteLine("Level 3: Handling request and resolving issue.");
        }
        else if (nextHandler != null)
        {
            nextHandler.HandleRequest(request);
        }
    }
}