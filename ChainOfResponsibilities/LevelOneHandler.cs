using System;

public class LevelOneHandler : Handler
{
    public override void HandleRequest(string request)
    {
        if (request == "1")
        {
            Console.WriteLine("Level 1: Handling request and resolving issue.");
        }
        else if (nextHandler != null)
        {
            nextHandler.HandleRequest(request);
        }
    }
}

